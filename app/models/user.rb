class User < ApplicationRecord
    has_many :tweet_posts
    has_many :user_groups
    has_many :groups,through: :user_groups
    validates :email,uniqueness: true
end
