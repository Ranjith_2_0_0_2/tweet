module Api
    class TweetsController < ApplicationController
      rescue_from ActiveRecord::RecordNotDestroyed,with: :not_destroyed
      def index
        render json: TweetPost.includes(:user).all
      end
      def create
        @tweet_post = TweetPost.new(tweet_post_params)
        if @tweet_post.save
          render json: @tweet_post,status: :created
        else
          render json:@tweet_post.errors,status: :unprocessable_entity
        end
      end
      def destroy
        TweetPost.find(params[:id]).destroy!
        render json:{success:true}
      end
      def update
        @tweet_post = TweetPost.find(params[:id])
        if @tweet_post.update!(tweet_post_params)
          render json:{success: true},status: :ok
        else
          render json: @tweet_post.error,status: :unprocessable_entity
        end
      end

      private
      def tweet_post_params
        params.require(:tweet).permit(:title,:body,:user_id)
      end
      def not_destroyed(e)
        render json:{error:e.record.errors},status: :unprocessable_entity
      end

  end
end
