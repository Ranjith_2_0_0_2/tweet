class UsersController < ApplicationController
    def index
        render json:User.all
    end
    def show
        @user=User.find(params[:id])
        render json:@user.groups,status: :ok
    end
end
