class TweetPost < ActiveRecord::Migration[7.0]
  def up
    drop_table :tweet_posts
    create_table :tweet_posts do |t|
      t.string :title
      t.text :body
      t.belongs_to :user,foreign_key:true

      t.timestamps
    end
  end
end
