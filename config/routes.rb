Rails.application.routes.draw do
  get 'group/index'
  get 'group/create'
  get 'group/destroy'
  get 'group/show'
  resources :groups
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    resources :tweets,only:[:index,:create,:destroy,:update]
  end
  resources :users,only:[:index,:create,:destroy,:show]
  resources :groups,only:[:index,:create,:destroy,:show]
end
